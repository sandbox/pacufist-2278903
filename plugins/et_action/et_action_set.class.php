<?php

/**
 * @file
 *
 * Et_action plugin class.
 */

class et_action_set extends et_action_basic {
  
  public function available() {
    return entity_translation_enabled($this->entity_type);
  }
  
  public function action($entity, $context, $handler = NULL) {

    $handler = $handler ? $handler : entity_translation_get_handler($this->entity_type, $entity);

    $lang_source = $this->entity_language($entity, $handler, FALSE);

    if ($lang_source == $this->options['target']) {
      return ENTITY_TRANSLATION_ACTIONS_RESULT_EQUAL;
    }

    $translations = $handler->getTranslations();

    // Add new translation if not existrs.
    if (!isset($translations->data[$this->options['target']]) && $lang_source && $this->options['target'] != LANGUAGE_NONE) {

      $options_add = array(
        'source' => $lang_source,
        'language' => $this->options['target'],
        'translation_exists' => ENTITY_TRANSLATION_ACTIONS_TRANSLATION_REPLACE,
      );
      
      $add_plugin = entity_translation_actions_plugin('et_action_add');
      $add_class = ctools_plugin_get_class($add_plugin, 'handler');
      if ($add_class) {
        $add_handler = new $add_class($this->entity_type, $options_add);
        $add_handler->action($entity, $context, $handler);
      }
    }

    $language_key = $handler->getLanguageKey();
    $translations->original = $this->options['target'];
    $entity->{$language_key} = $this->options['target'];

    foreach ($translations->data as &$translation) {
      if ($translation['language'] == LANGUAGE_NONE && $this->options['target'] != LANGUAGE_NONE) {
        $handler->removeTranslation($translation['language']);
      }
      elseif ($translation['language'] == $this->options['target']) {
        $translation['source'] = FALSE;
        $handler->setTranslation($translation);
      }
      else {
        $translation['source'] = $this->options['target'];
        $handler->setTranslation($translation);
      }
      unset($translation);
    }

    return ENTITY_TRANSLATION_ACTIONS_RESULT_REPLACED;
  }

  public function form_build(&$form, &$form_state) {

    $form['target'] = array(
      '#type' => 'radios',
      '#options' => $this->languages_list(),
      '#title' => t('New entity language'),
      '#required' => TRUE,
    );
  }

}
