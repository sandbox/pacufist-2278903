<?php

/**
 * @file
 *
 * CCT plugin file.
 */
$plugin = array(
  'title' => t('Translation Add'),
  'description' => t('Add copy of existed translation for a new language.'),
  'handler' => array(
    'class' => 'et_action_add',
    'parent' => 'et_action_basic',
  ),
  'single' => TRUE,
  'icon' => 'no-icon.png',
  'weight' => 1,
);
