<?php

/**
 * @file
 *
 * CCT plugin file.
 */
$plugin = array(
  'title' => t('Translation Basic'),
  'description' => t('Provide basic class for all translation action plugins.'),
  'handler' => array(
    'class' => 'et_action_basic',
  ),
  'single' => TRUE,
  'icon' => 'no-icon.png',
  'weight' => 0,
);
