<?php

/**
 * @file
 *
 * CCT plugin file.
 */

$plugin = array(
  'title' => t('Translation Set'),
  'description' => t('Set new language for entity.'),
  'handler' => array(
    'class' => 'et_action_set',
    'parent' => 'et_action_basic',
  ),
  'single' => TRUE,
  'icon' => 'no-icon.png',
  'weight' => 2,
);
