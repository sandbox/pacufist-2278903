<?php

/**
 * @file
 *
 * Et_action plugin class.
 */

class et_action_add extends et_action_basic {
  
  public function available() {
    return entity_translation_enabled($this->entity_type);
  }
  
  public function action($entity, $context, $handler = NULL) {

    $handler = $handler ? $handler : entity_translation_get_handler($this->entity_type, $entity);

    $lang_source = ($this->options['source'] == '-entity-') ? $this->entity_language($entity, $handler, FALSE) : $this->options['source'];
    $language_target = ($this->options['language'] == '-entity-') ? $this->entity_language($entity, $handler, FALSE) : $this->options['language'];

    if ($lang_source == $language_target) {
      return;
    }

    $translations = $handler->getTranslations();

    // No source translation;
    if ((!$lang_source || !$translations || !isset($translations->data[$lang_source])) && $lang_source != LANGUAGE_NONE) {
      return ENTITY_TRANSLATION_ACTIONS_RESULT_NO_SOURCE;
    }

    // Target translation exists;
    if (isset($translations->data[$language_target]) && $this->options['translation_exists'] == ENTITY_TRANSLATION_ACTIONS_TRANSLATION_SKIP) {
      return ENTITY_TRANSLATION_ACTIONS_RESULT_EXISTS;
    }

    list(,, $bundle_name) = entity_extract_ids($this->entity_type, $entity);

    // Clone field translations.
    foreach (field_info_instances($this->entity_type, $bundle_name) as $instance) {
      $field_name = $instance['field_name'];
      $field = field_info_field($field_name);

      if ($field['translatable']) {
        $original_item = isset($entity->{$field_name}[$lang_source]) ? $entity->{$field_name}[$lang_source] : NULL;

        // MERGE. Add item if not empty and no value for language.
        if ($original_item && !isset($entity->{$field_name}[$language_target])) {
          $entity->{$field_name}[$language_target] = $original_item;
        }
        // REPLACE. Replace existed value for language. Or remode it if source values is empty.
        elseif ($this->options['translation_exists'] == ENTITY_TRANSLATION_ACTIONS_TRANSLATION_REPLACE) {
          if ($original_item) {
            $entity->{$field_name}[$language_target] = $original_item;
          }
          else {
            unset($entity->{$field_name}[$language_target]);
          }
        }
      }
    }

    // Create new translation.
    if ($language_target != LANGUAGE_NONE) {
      $lang_entity = $handler->getLanguage();
      $new_translation = isset($translations->data[$lang_source]) ? $translations->data[$lang_source] : array();
      $new_translation['status'] = TRUE;
      $new_translation['language'] = $language_target;
      $new_translation['source'] = ($lang_entity == $language_target) ? FALSE : $lang_entity;
      $handler->setTranslation($new_translation);
    }
    return ENTITY_TRANSLATION_ACTIONS_RESULT_CREATED;
  }
  
  public function form_build(&$form, &$form_state) {
    
    $options = $this->languages_options();
    
    $form['language'] = array(
      '#type' => 'radios',
      '#options' => $options,
      '#title' => t('Add translations for language'),
      '#required' => TRUE,
    );
    
    $options_keys = array_keys($options);
    
    $form['source'] = array(
      '#type' => 'radios',
      '#options' => $options,
      '#title' => t('From language'),
      '#required' => TRUE,
      '#default_value' => current($options_keys),
    );

    $form['translation_exists'] = array(
      '#type' => 'select',
      '#options' => $this->translation_exists_options(),
      '#title' => t('Actions for existed translation'),
    );
    
  }
  
  protected function translation_exists_options() {
    return array(
      ENTITY_TRANSLATION_ACTIONS_TRANSLATION_SKIP => t('SKIP any actions if translation existed'),
      ENTITY_TRANSLATION_ACTIONS_TRANSLATION_REPLACE => t('Replace ALL existed values with new'),
      ENTITY_TRANSLATION_ACTIONS_TRANSLATION_MERGE => t('Copy value if existed is EMPTY'),
    );
  }
}
