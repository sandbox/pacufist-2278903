<?php

/**
 * @file
 *
 * Et_action plugin class.
 */

class et_action_delete extends et_action_basic {

  public function available() {
    return entity_translation_enabled($this->entity_type);
  }
  
  public function action($entity, $context, $handler = NULL) {
    $handler = $handler ? $handler : entity_translation_get_handler($this->entity_type, $entity);
    
    $translations = $handler->getTranslations();
    // Set new original language if replaced language was original.
    $lang_entity = $this->entity_language($entity, $handler, FALSE);
    if ($lang_entity && in_array($lang_entity, $this->options['language'])) {

      $language_key = $handler->getLanguageKey();
      $translations->original = LANGUAGE_NONE;
      $entity->{$language_key} = LANGUAGE_NONE;
      
      foreach ($translations->data as &$translation) {
        if ($translation['language'] == LANGUAGE_NONE) {
          $translation['source'] = FALSE;
          $handler->setTranslation($translation);
        }
        elseif ($translation['source'] == $lang_entity) {
          $translation['source'] = LANGUAGE_NONE;
          $handler->setTranslation($translation);
        }
        unset($translation);
      }
    }

    foreach ($this->options['language'] as $language) {
      $handler->removeTranslation($language);
    }
    
    return ENTITY_ACTIONS_KIT_RESULT_DELETED;
  }
  
  public function form_build(&$form, &$form_state) {
    $form['language'] = array(
      '#type' => 'checkboxes',
      '#options' => $this->languages_list(),
      '#title' => t('Delete translation for languages'),
      '#required' => TRUE,
    );
  }
  
   public function form_submit($form, &$form_state, $options) {
     parent::form_submit($form, $form_state, $options);
     $this->options['language'] = array_filter($this->options['language']);
  }
}