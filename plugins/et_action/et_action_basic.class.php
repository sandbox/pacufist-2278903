<?php

/**
 * @file
 *
 * Et_action plugin class.
 */

interface entityTranslationActionEtAction {
  public function available();
  public function action($entity, $context, $handler = NULL);
  public function form_build(&$form, &$form_state);
  public function form_validate($form, &$form_state);
  public function form_submit($form, &$form_state, $options);
  public function entity_language($entity, $handler, $default);
}

class et_action_basic implements entityTranslationActionEtAction {

  public $entity_type;
  public $options;

  public function __construct($entity_type, $options) {
    $this->entity_type = $entity_type;
    $this->options = $options;
  }
  
  public function available() {
    return FALSE;
  }
  
  public function action($entity, $context, $handler = NULL) {
    
  }

  public function form_build(&$form, &$form_state) {

  }

  public function form_validate($form, &$form_state) {
    
  }

  public function form_submit($form, &$form_state, $options) {
    $this->options = $options;
  }
  
  public function entity_language($entity, $handler, $default) {
    $language_key = $handler->getLanguageKey();
    if ($language_key) {
      if (isset($entity->{$language_key}) && !empty($entity->{$language_key})) {
        return $entity->{$language_key};
      }
    }

    $translations = $handler->getTranslations();
    if (!empty($translations->original)) {
      return $translations->original;
    }
    else {
      return $default;
    }
  }
  
  protected function languages_list() {
    
    $languages = entity_translation_languages();

    $language_none = array(LANGUAGE_NONE => t('Language neutral'));
    $languages_list = $language_none;

    foreach ($languages as $language) {
      $languages_list[$language->language] = $language->name;
    }
    
    return $languages_list;
  }
  
  protected function languages_options() {

    $languages_options = array(
      '-entity-' => t('Entity language'),
        ) + $this->languages_list();

    return $languages_options;
  }

}