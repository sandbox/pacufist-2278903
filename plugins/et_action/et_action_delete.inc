<?php

/**
 * @file
 *
 * CCT plugin file.
 */
$plugin = array(
  'title' => t('Translation Delete'),
  'description' => t('Delete copy of existed translation for a new language.'),
  'handler' => array(
    'class' => 'et_action_delete',
    'parent' => 'et_action_basic',
  ),
  'single' => TRUE,
  'icon' => 'no-icon.png',
  'weight' => 3,
);
